package edu.ait.inventoryservice.controller;

import edu.ait.finalproject.dto.inventoryservice.Stock;
import edu.ait.inventoryservice.configuration.Configuration;
import edu.ait.inventoryservice.repository.BookRepository;
import edu.ait.finalproject.dto.inventoryservice.Book;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(InventoryServiceController.class)
class InventoryServiceControllerTest {

    @MockBean
    private BookRepository bookRepository;

    @MockBean
    Configuration config;

    @Autowired
    private MockMvc mockMvc;

    private Book book;

    private List<Book> bookList = new ArrayList<>();

    private Stock stock;

    LocalDate testDate = LocalDate.of(2000,01,01);

    @BeforeEach
    void setUp() {
        RestAssuredMockMvc.mockMvc(mockMvc);
        stock = new Stock(100, 12.34F, 200, 43.21F, Integer.valueOf(1234));
        book = new Book("Test Book Title","Test Arthur McAuthor","Test Book Publisher", testDate, 1,"123456789","Test Book Description",stock );
        bookList.add(book);
    }

    @Test
    void getBookById(){
        Mockito.when(bookRepository.findById(1)).thenReturn(Optional.ofNullable(book));

        RestAssuredMockMvc
                .given()
                .when()
                .get("/books/1")
                .then()
                .statusCode(200)
                .body("id",Matchers.equalTo(null))
                .body("title", Matchers.equalTo("Test Book Title"))
                .body("author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("publisher", Matchers.equalTo("Test Book Publisher"))
                .body("publishDate", Matchers.equalTo("2000-01-01"))
                .body("edition", Matchers.equalTo(1))
                .body("isbn", Matchers.equalTo("123456789"))
                .body("description", Matchers.equalTo("Test Book Description"))
                .body("stock.id", Matchers.equalTo(stock.getId()))
                .body("stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void getBookByAuthorSearch(){
        Mockito.when(bookRepository.findByAuthorContainingIgnoreCase("Arthur")).thenReturn(bookList);

        RestAssuredMockMvc
                .given()
                .param("author", "Arthur")
                .when()
                .get("/books")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(null))
                .body("[0].title", Matchers.equalTo("Test Book Title"))
                .body("[0].author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("[0].publisher", Matchers.equalTo("Test Book Publisher"))
                .body("[0].publishDate", Matchers.equalTo("2000-01-01"))
                .body("[0].edition", Matchers.equalTo(1))
                .body("[0].isbn", Matchers.equalTo("123456789"))
                .body("[0].description", Matchers.equalTo("Test Book Description"))
                .body("[0].stock.id", Matchers.equalTo(stock.getId()))
                .body("[0].stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("[0].stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("[0].stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("[0].stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("[0].stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void getBookByTitleSearch(){
        Mockito.when(bookRepository.findByTitleContainingIgnoreCase("Test")).thenReturn(bookList);

        RestAssuredMockMvc
                .given()
                .param("title", "Test")
                .when()
                .get("/books")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(null))
                .body("[0].title", Matchers.equalTo("Test Book Title"))
                .body("[0].author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("[0].publisher", Matchers.equalTo("Test Book Publisher"))
                .body("[0].publishDate", Matchers.equalTo("2000-01-01"))
                .body("[0].edition", Matchers.equalTo(1))
                .body("[0].isbn", Matchers.equalTo("123456789"))
                .body("[0].description", Matchers.equalTo("Test Book Description"))
                .body("[0].stock.id", Matchers.equalTo(stock.getId()))
                .body("[0].stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("[0].stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("[0].stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("[0].stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("[0].stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void getBookByISBNSearch(){
        Mockito.when(bookRepository.findByIsbnIs("123456789")).thenReturn(bookList);

        RestAssuredMockMvc
                .given()
                .param("isbn", "123456789")
                .when()
                .get("/books")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(null))
                .body("[0].title", Matchers.equalTo("Test Book Title"))
                .body("[0].author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("[0].publisher", Matchers.equalTo("Test Book Publisher"))
                .body("[0].publishDate", Matchers.equalTo("2000-01-01"))
                .body("[0].edition", Matchers.equalTo(1))
                .body("[0].isbn", Matchers.equalTo("123456789"))
                .body("[0].description", Matchers.equalTo("Test Book Description"))
                .body("[0].stock.id", Matchers.equalTo(stock.getId()))
                .body("[0].stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("[0].stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("[0].stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("[0].stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("[0].stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void getBookByPublisherSearch(){
        Mockito.when(bookRepository.findByPublisherContainingIgnoreCase("Publisher")).thenReturn(bookList);

        RestAssuredMockMvc
                .given()
                .param("publisher", "Publisher")
                .when()
                .get("/books")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(null))
                .body("[0].title", Matchers.equalTo("Test Book Title"))
                .body("[0].author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("[0].publisher", Matchers.equalTo("Test Book Publisher"))
                .body("[0].publishDate", Matchers.equalTo("2000-01-01"))
                .body("[0].edition", Matchers.equalTo(1))
                .body("[0].isbn", Matchers.equalTo("123456789"))
                .body("[0].description", Matchers.equalTo("Test Book Description"))
                .body("[0].stock.id", Matchers.equalTo(stock.getId()))
                .body("[0].stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("[0].stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("[0].stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("[0].stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("[0].stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void getBookByTitleAndAuthorSearch(){
        Mockito.when(bookRepository.findByTitleContainingIgnoreCaseAndAuthorContainingIgnoreCase("Test", "Arthur")).thenReturn(bookList);

        RestAssuredMockMvc
                .given()
                .param("title", "Test")
                .param("author", "Arthur")
                .when()
                .get("/books")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(null))
                .body("[0].title", Matchers.equalTo("Test Book Title"))
                .body("[0].author", Matchers.equalTo("Test Arthur McAuthor"))
                .body("[0].publisher", Matchers.equalTo("Test Book Publisher"))
                .body("[0].publishDate", Matchers.equalTo("2000-01-01"))
                .body("[0].edition", Matchers.equalTo(1))
                .body("[0].isbn", Matchers.equalTo("123456789"))
                .body("[0].description", Matchers.equalTo("Test Book Description"))
                .body("[0].stock.id", Matchers.equalTo(stock.getId()))
                .body("[0].stock.initialQuantity", Matchers.equalTo(stock.getInitialQuantity()))
                .body("[0].stock.initialPrice", Matchers.equalTo(stock.getInitialPrice()))
                .body("[0].stock.currentQuantity", Matchers.equalTo(stock.getCurrentQuantity()))
                .body("[0].stock.currentPrice", Matchers.equalTo(stock.getCurrentPrice()))
                .body("[0].stock.supplierId", Matchers.equalTo(stock.getSupplierId()));
    }

    @Test
    void postBookTest(){

        Mockito.when(bookRepository.save(any(Book.class))).thenReturn(book);
        RestAssuredMockMvc
                .given()
                .contentType("application/json")
                .body("{\n" +
                        "    \"title\": \"Spring Microservices in Action\",\n" +
                        "    \"author\": \"John Carnell\",\n" +
                        "    \"publisher\": \"Manning\",\n" +
                        "    \"publishDate\": \"2017-07-31\",\n" +
                        "    \"edition\": 1,\n" +
                        "    \"isbn\": \"9781617293986\",\n" +
                        "    \"description\": \"Spring Microservices in Action teaches you how to build microservice-based applications using Java and the Spring platform. You'll learn to do microservice design as you build and deploy your first Spring Cloud application.\",\n" +
                        "    \"stock\": {\n" +
                        "        \"initialQuantity\": 20,\n" +
                        "        \"initialPrice\": 23.5,\n" +
                        "        \"currentQuantity\": 11,\n" +
                        "        \"currentPrice\": 22.5,\n" +
                        "        \"supplierId\": 1234\n" +
                        "    }\n" +
                        "}")
                .when()
                .post("/books")
                .then()
                .statusCode(201)
                .header("Location", Matchers.containsString("http://localhost/books"));

    }

    @Test
    void deleteBookById(){
        Mockito.doNothing().when(bookRepository).deleteById(1);
        RestAssuredMockMvc
                .given()
                .when()
                .delete("/books/1")
                .then()
                .statusCode(200);
    }
}