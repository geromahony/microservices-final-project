package edu.ait.shippingservice.FeignClient;

import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.inventoryservice.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient("zuul-api-gateway")
public interface FeignClientApiGateway {

    @GetMapping("/customer-service/customers/{id}")
    Customer getCustomerById(@PathVariable Long id);

    @PutMapping("/customer-service/customers")
    ResponseEntity updateCustomer(@RequestBody Customer customer);

    @GetMapping("/inventory-service/books/{id}")
    Book getBookById(@PathVariable int id);

    @PutMapping("/inventory-service/books")
    ResponseEntity updateBook(@RequestBody Book book);

}
