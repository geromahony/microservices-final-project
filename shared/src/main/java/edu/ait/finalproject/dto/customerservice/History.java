package edu.ait.finalproject.dto.customerservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Entity
@Table(name="History")
@ApiModel(description = "Bookshop Customer History Details -  A history entity with prior customer order history of order id, date and cost")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ApiModelProperty(name = "orderId", dataType = "Long", example = "12345", notes="Order Id from inventory service")
    private Long orderId;

    @PastOrPresent
    @ApiModelProperty(name = "orderDate", dataType = "Date", example = "12/10/2020", notes="Order date")
    private java.time.LocalDate orderDate;

    @Positive
    @ApiModelProperty(name = "orderCost", dataType = "Float", example = "18.55", notes="Order cost")
    private float orderCost;

    public History() {
    }

    public History(Long orderId, LocalDate orderDate, float orderCost) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.orderCost = orderCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public float getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(float orderCost) {
        this.orderCost = orderCost;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", orderDate=" + orderDate +
                ", orderCost=" + orderCost +
                '}';
    }
}

