package edu.ait.shippingservice.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ShipmentNotFound extends Exception
{
    public ShipmentNotFound(String message)
    {
        super(message);
    }
}