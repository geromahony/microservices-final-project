package edu.ait.finalproject.dto.inventoryservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name="Stocks")
@ApiModel(description = "Bookshop Book Stock Details - A stock entity with initial & current quantities and price of books in the shop")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @PositiveOrZero
    @ApiModelProperty(name = "initialQuantity", dataType = "Integer", example = "5", notes="Must be zero or positive integer")
    private int initialQuantity;

    @PositiveOrZero
    @ApiModelProperty(name = "initialPrice", dataType = "Float", example = "25.75", notes="Must be zero or positive floating point number")
    private float initialPrice;

    @PositiveOrZero
    @ApiModelProperty(name = "currentQuantity", dataType = "Integer", example = "5", notes="Must be zero or positive integer")
    private int currentQuantity;

    @PositiveOrZero
    @ApiModelProperty(name = "currentPrice", dataType = "Float", example = "35.25", notes="Must be zero or positive floating point number")
    private float currentPrice;

    @Positive
    private Integer supplierId;

    @OneToOne(mappedBy = "stock")
    private Book book;

    public Stock() {
    }

    public Stock(int initialQuantity, float initialPrice, int currentQuantity, float currentPrice, Integer supplierId) {
        this.initialQuantity = initialQuantity;
        this.initialPrice = initialPrice;
        this.currentQuantity = currentQuantity;
        this.currentPrice = currentPrice;
        this.supplierId = supplierId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getInitialQuantity() {
        return initialQuantity;
    }

    public void setInitialQuantity(int initialQuantity) {
        this.initialQuantity = initialQuantity;
    }

    public float getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(float initialPrice) {
        this.initialPrice = initialPrice;
    }

    public int getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(int currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }
}

