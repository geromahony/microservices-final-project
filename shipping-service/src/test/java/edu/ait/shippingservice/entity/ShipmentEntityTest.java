package edu.ait.shippingservice.entity;

import edu.ait.finalproject.dto.shippingservice.Shipment;
import edu.ait.shippingservice.repository.ShippingRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ShipmentEntityTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ShippingRepository shippingRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    LocalDate testDate1 = LocalDate.of(2000,01,01);
    LocalDate testDate2 = LocalDate.of(2030,01,01);

    private Shipment shipment;

    @Before
    public void setUp(){
        shipment = new Shipment(1L,1,"John Doe", "The Test Address", "john.doe@gamil.com", testDate1, testDate2);
    }

    @Test
    public void saveBookEntity(){
        Shipment savedShipment = this.entityManager.persistAndFlush(shipment);
        assertThat(savedShipment.getId()).isNotNull();
        assertThat(savedShipment.getCustomerId()).isEqualTo(1L);
        assertThat(savedShipment.getBookId()).isEqualTo(1);
        assertThat(savedShipment.getCustomerName()).isEqualTo("John Doe");
        assertThat(savedShipment.getCustomerAddress()).isEqualTo("The Test Address");
        assertThat(savedShipment.getCustomerEmail()).isEqualTo("john.doe@gamil.com");
        assertThat(savedShipment.getOrderReceivedDate()).isEqualTo(testDate1);
        assertThat(savedShipment.getOrderCompletionDate()).isEqualTo(testDate2);
    }
}
