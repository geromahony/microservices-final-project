package edu.ait.inventoryservice.repository;

import edu.ait.finalproject.dto.inventoryservice.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book,Integer> {
    // Derived Query - search for books by title and author containing and ignore case
    List<Book> findByTitleContainingIgnoreCaseAndAuthorContainingIgnoreCase(String title, String author);

    // Derived Query - search by title ignoring case
    List<Book> findByTitleContainingIgnoreCase(String title);

    // Derived Query - find by author ignoring case
    List<Book> findByAuthorContainingIgnoreCase(String author);

    // Derived Query - find by ISBN
    List<Book> findByIsbnIs(String isbn);

    // Derived Query - find by publisher
    List<Book> findByPublisherContainingIgnoreCase(String publisher);
}
