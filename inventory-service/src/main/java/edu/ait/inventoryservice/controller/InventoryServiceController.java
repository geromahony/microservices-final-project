package edu.ait.inventoryservice.controller;

import edu.ait.inventoryservice.configuration.Configuration;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.inventoryservice.exceptions.ResourceNotFoundException;
import edu.ait.inventoryservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class InventoryServiceController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    Configuration config;

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getBooks(@RequestParam("title") Optional<String> bookTitle,
                               @RequestParam("author") Optional<String> bookAuthor,
                               @RequestParam("isbn") Optional<String> bookISBN,
                               @RequestParam("publisher") Optional<String> bookPublisher) throws ResourceNotFoundException {
        List<Book> books = null;

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri();

        HttpHeaders responseHeaders = new HttpHeaders();

        // Create custom response header to return configuration parameters
        responseHeaders.add("Environment", config.getEnvironment());
        responseHeaders.add("Version", config.getVersion());

        if (bookTitle.isPresent() && bookAuthor.isPresent()) {

            books = bookRepository.findByTitleContainingIgnoreCaseAndAuthorContainingIgnoreCase(bookTitle.get(), bookAuthor.get());

            if (books.isEmpty()) {
                throw new ResourceNotFoundException("Books not found by author " + bookAuthor.get() + " with title containing: " + bookTitle.get());
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .location(location)
                        .headers(responseHeaders)
                        .body(books);
            }
        }

        if (bookTitle.isPresent()) {
            books = bookRepository.findByTitleContainingIgnoreCase(bookTitle.get());
            if (books.isEmpty()) {
                throw new ResourceNotFoundException("Books not found with title containing: " + bookTitle.get());
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .location(location)
                        .headers(responseHeaders)
                        .body(books);
            }
        }

        if (bookAuthor.isPresent()) {
            books = bookRepository.findByAuthorContainingIgnoreCase(bookAuthor.get());
            if (books.isEmpty()) {
                throw new ResourceNotFoundException("Books not found by author: " + bookAuthor.get());
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .location(location)
                        .headers(responseHeaders)
                        .body(books);
            }
        }

        if (bookISBN.isPresent()) {
            books = bookRepository.findByIsbnIs(bookISBN.get());
            if (books.isEmpty()) {
                throw new ResourceNotFoundException("Books not found with ISBN: " + bookISBN.get());
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .location(location)
                        .headers(responseHeaders)
                        .body(books);
            }
        }

        if (bookPublisher.isPresent()) {
            books = bookRepository.findByPublisherContainingIgnoreCase(bookPublisher.get());
            if (books.isEmpty()) {
                throw new ResourceNotFoundException("Books not found by publisher: " + bookPublisher.get());
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .location(location)
                        .headers(responseHeaders)
                        .body(books);
            }
        }

        return ResponseEntity.status(HttpStatus.OK)
                .location(location)
                .headers(responseHeaders)
                .body(bookRepository.findAll());
    }

    @GetMapping("/books/{id}")
    public Book getBookById(@Valid @PathVariable int id) throws ResourceNotFoundException {
        Optional<Book> foundBook = bookRepository.findById(id);
        if( foundBook.isPresent())
            return foundBook.get();
        else
            throw new ResourceNotFoundException("Unable to find book with id: " + id);
    }

    @DeleteMapping("/books/{id}")
    public void deleteBook(@Valid @PathVariable(value = "id") Integer bookId) throws ResourceNotFoundException {

        try {
            bookRepository.deleteById(bookId);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("Unable to delete book with id: " + bookId);
        }
    }

    @PostMapping("/books")
    public ResponseEntity postBook(@Valid @RequestBody Book book) {
        Book savedBook = bookRepository.save(book);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("{id}")
                .buildAndExpand(savedBook.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/books")
    public ResponseEntity updateBook(@Valid @RequestBody Optional<Book> book,
                                     @RequestParam("id") Optional<Integer> bookId,
                                     @RequestParam("quantity") Optional<Integer> bookQuantity) {

        if(book.isPresent()){
            if (book.get().getId() != null) {
                bookRepository.save(book.get());

                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                Book savedBook = bookRepository.save(book.get());

                URI location = ServletUriComponentsBuilder
                        .fromCurrentRequest().path("{id}")
                        .buildAndExpand(savedBook.getId())
                        .toUri();
                return ResponseEntity.created(location).build();
            }

        }

        if(bookId.isPresent() && bookQuantity.isPresent()){
            Optional<Book> bookToUpdate = bookRepository.findById(bookId.get());

            if(bookToUpdate.isPresent()){
                bookToUpdate.get().getStock().setCurrentQuantity(bookQuantity.get());
                bookRepository.save(bookToUpdate.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
