package edu.ait.bookshopclient.FeignClients;

import edu.ait.bookshopclient.feignformatters.OptionalFeignFormatterRegistrar;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.shippingservice.Shipment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@FeignClient("zuul-api-gateway")
public interface FeignClientApiGateway {

    @GetMapping("/customer-service/customers")
    List<Customer> getAllCustomers();

    @GetMapping("/customer-service/customers/{id}")
    Customer getCustomerById(@PathVariable Long id);

    @GetMapping("/inventory-service/books")
    ResponseEntity<List<Book>> getBooks(@RequestParam("title") Optional<String> bookTitle,
                        @RequestParam("author") Optional<String> bookAuthor,
                        @RequestParam("isbn") Optional<String> bookISBN,
                        @RequestParam("publisher") Optional<String> bookPublisher);

    @GetMapping("/inventory-service/books/{id}")
    Book getBookById(@PathVariable int id);

    @PostMapping("/inventory-service/books")
    ResponseEntity postBook(@RequestBody Book book);

    @PutMapping("/inventory-service/books")
    ResponseEntity updateBook(@RequestBody Optional<Book> book,
                              @RequestParam("id") Optional<Integer> bookId,
                              @RequestParam("quantity") Optional<Integer> bookQuantity);

    @DeleteMapping("/inventory-service/books/{id}")
    void deleteBook(@PathVariable int id);

    @PostMapping("/shipping-service/shipments")
    ResponseEntity postShipment(@RequestParam("customer") Long customerId, @RequestParam("book") Integer bookId);

    @GetMapping("/shipping-service/shipments")
    List<Shipment> getAllShipments();
}
