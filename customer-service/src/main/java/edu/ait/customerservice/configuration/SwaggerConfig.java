package edu.ait.customerservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final Contact CUSTOM_CONTACT = new Contact ("Ger O'Mahony", "edu.ait.mybookshop.customer", "a00288490@student.ait.ie" );
    public static final ApiInfo CUSTOM_API_INFO = new ApiInfo ( "BookShop Customer REST API Documentation", "My Bookshop Customer Swagger Documentation","1.0", "", CUSTOM_CONTACT, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<VendorExtension>());

    @Bean
    public Docket swaggerApi(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(CUSTOM_API_INFO);
    }
}
