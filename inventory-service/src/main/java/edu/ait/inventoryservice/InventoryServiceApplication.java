package edu.ait.inventoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableDiscoveryClient
@Configuration
@EntityScan("edu.ait.finalproject.dto.inventoryservice")
public class InventoryServiceApplication { public static void main(String[] args) {SpringApplication.run(InventoryServiceApplication.class, args);
	}
}
