package edu.ait.inventoryservice.repository;

import edu.ait.finalproject.dto.inventoryservice.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {
}
