package edu.ait.bookshopclient.controllers;

import edu.ait.bookshopclient.FeignClients.FeignClientApiGateway;
import edu.ait.bookshopclient.exceptions.ResourceNotFoundException;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.inventoryservice.Stock;
import edu.ait.finalproject.dto.shippingservice.Shipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BookShopClientFeignController {
    @Autowired
    private FeignClientApiGateway feignClientApiGateway;

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getBooksFromInventoryService(@RequestParam("title") Optional<String> bookTitle,
                                                                   @RequestParam("author") Optional<String> bookAuthor,
                                                                   @RequestParam("isbn") Optional<String> bookISBN,
                                                                   @RequestParam("publisher") Optional<String> bookPublisher) throws ResourceNotFoundException {

        // Feign client to inventory-service
        ResponseEntity<List<Book>> inventoryBooks = feignClientApiGateway.getBooks(bookTitle,bookAuthor,bookISBN,bookPublisher);

        if( inventoryBooks.getBody().isEmpty()){
            throw new ResourceNotFoundException("No books found");
        } else {
            for (Book book : inventoryBooks.getBody()){
                book.getStock().setInitialPrice(0.0F);
                book.getStock().setSupplierId(0);
            }
        }
        return inventoryBooks;
    }

    @PostMapping("/customers/orders")
    public ResponseEntity customerOrder(@RequestParam("customer") Long customerId, @RequestParam("book") Integer bookId ) throws ResourceNotFoundException {

        Customer customerOrdering;
        Book bookOrder;
        // Feign client to customer-service to check customer exists first
        try{
            customerOrdering = feignClientApiGateway.getCustomerById(customerId);
        } catch (Exception customerException){
            throw new ResourceNotFoundException("No customer found with id: " + customerId);
        }
        // Feign client to inventory-service to check book exists first
        try{
            bookOrder = feignClientApiGateway.getBookById(bookId);
        } catch (Exception inventoryException){
            throw new ResourceNotFoundException("No book found with id: " + bookId);
        }

        // post to shipping-service to generate order
        if(customerOrdering.getId() != null && bookOrder.getId() != null){
            return feignClientApiGateway.postShipment(customerId,bookId);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/admin/customers")
    public List<Customer> getCustomersFromCustomerService(){
        return feignClientApiGateway.getAllCustomers();
    }

    @GetMapping("/admin/customers/{id}")
    public Customer getCustomerByIdFromCustomerService(@PathVariable Long id){
        return feignClientApiGateway.getCustomerById(id);
    }

    @GetMapping("/admin/books")
    public ResponseEntity<List<Book>> getAdminBooksFromInventoryService(@RequestParam("title") Optional<String> bookTitle,
                                                        @RequestParam("author") Optional<String> bookAuthor,
                                                        @RequestParam("isbn") Optional<String> bookISBN,
                                                        @RequestParam("publisher") Optional<String> bookPublisher){
        return feignClientApiGateway.getBooks(bookTitle, bookAuthor, bookISBN, bookPublisher);
    }

    @PostMapping("/admin/books")
    public ResponseEntity postBook(@RequestBody Book book) {
        return feignClientApiGateway.postBook(book);
    }

    @PutMapping("/admin/books")
    public ResponseEntity updateBook(@RequestBody Optional<Book> book,
                                     @RequestParam("id") Optional<Integer> bookId,
                                     @RequestParam("quantity") Optional<Integer> bookQuantity){
        return feignClientApiGateway.updateBook(book,bookId,bookQuantity);
    }

    @DeleteMapping("/admin/books/{id}")
    public void deleteAdminBookFromInventoryService(@PathVariable int id){
        feignClientApiGateway.deleteBook(id);
    }

    @GetMapping("/admin/shipments")
    public List<Shipment> getShipmentsFromShipmentService(){
        return feignClientApiGateway.getAllShipments();
    }
}
