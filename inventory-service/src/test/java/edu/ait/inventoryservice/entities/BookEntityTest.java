package edu.ait.inventoryservice.entities;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.inventoryservice.Stock;

import edu.ait.inventoryservice.repository.BookRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BookEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BookRepository bookRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    LocalDate testDate = LocalDate.of(2000,01,01);

    private Book book;

    private Stock stock;

    @Before
    public void setUp(){
        stock = new Stock(100, 12.34F, 200, 43.21F, Integer.valueOf(1234));
        book = new Book("Test Book Title","Test Arthur McAuthor","Test Book Publisher", testDate, 1,"123456789","Test Book Description",stock );
    }

    @Test
    public void injectedComponentsAreNotNull(){
        assertThat(entityManager).isNotNull();
        assertThat(bookRepository).isNotNull();
    }

    @Test
    public void saveStockEntity(){
        Stock savedStock = this.entityManager.persistAndFlush(stock);
        assertThat(savedStock.getId()).isNotNull();
        assertThat(savedStock.getInitialQuantity()).isEqualTo(100);
        assertThat(savedStock.getInitialPrice()).isEqualTo(12.34F);
        assertThat(savedStock.getCurrentQuantity()).isEqualTo(200);
        assertThat(savedStock.getCurrentPrice()).isEqualTo(43.21F);
        assertThat(savedStock.getSupplierId()).isEqualTo(1234);
    }

    @Test
    public void saveBookEntity(){
        Book savedBook = this.entityManager.persistAndFlush(book);
        assertThat(savedBook.getId()).isNotNull();
        assertThat(savedBook.getTitle()).isEqualTo("Test Book Title");
        assertThat(savedBook.getAuthor()).isEqualTo("Test Arthur McAuthor");
        assertThat(savedBook.getPublisher()).isEqualTo("Test Book Publisher");
        assertThat(savedBook.getPublishDate()).isEqualTo(testDate);
        assertThat(savedBook.getEdition()).isEqualTo(1);
        assertThat(savedBook.getIsbn()).isEqualTo("123456789");
        assertThat(savedBook.getDescription()).isEqualTo("Test Book Description");
        assertThat(savedBook.getStock()).isEqualTo(stock);
    }

}
