package edu.ait.shippingservice.repository;

import edu.ait.finalproject.dto.shippingservice.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingRepository extends JpaRepository<Shipment,Long> {
}
