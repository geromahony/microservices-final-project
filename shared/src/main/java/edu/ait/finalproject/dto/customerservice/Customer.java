package edu.ait.finalproject.dto.customerservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "Customers")
@ApiModel(description = "Bookshop Customer Details -  A customer entity with customer name, address and email also including prior order history")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min=3)
    @ApiModelProperty(name = "firstName", dataType = "String", example = "John", notes="First name cannot be initial, must be minimum of three characters")
    private String firstName;

    @NotEmpty
    @ApiModelProperty(name = "lastName", dataType = "String", example = "O'Connor")
    private String lastName;

    @Email(message = "Email should be valid")
    @ApiModelProperty(name = "email", dataType = "String", example = "john-doe@mymail.com", notes="Should be a valid email address")
    private String email;

    @Lob
    @ApiModelProperty(name = "address", dataType = "String", example = "1 Mill Lane, Milltown, County Louth", notes="Should be a valid address")
    private String address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "customer_history",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "history_id"))
    private List<History> history;

    public Customer() {
    }

    public Customer(Long id, String firstName, String lastName, String email, String address, List<History> history) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.history = history;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", history=" + history +
                '}';
    }
}

