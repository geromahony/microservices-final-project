package edu.ait.shippingservice.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.customerservice.History;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.shippingservice.Shipment;
import edu.ait.shippingservice.FeignClient.FeignClientApiGateway;
import edu.ait.shippingservice.exceptions.ShipmentNotFound;
import edu.ait.shippingservice.repository.ShippingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.net.URI;

@RestController
public class ShippingServiceController {

    @Autowired
    private FeignClientApiGateway feignClientApiGateway;

    @Autowired
    private ShippingRepository shippingRepository;

    @GetMapping("/shipments")
    public List<Shipment> getAllShipments(){
        return shippingRepository.findAll();
    }

    @GetMapping("/shipments/{id}")
    public ResponseEntity<Shipment> getShipmentById(@PathVariable(value = "id") Long shipmentId) throws ShipmentNotFound {
        Optional<Shipment> shipment = shippingRepository.findById(shipmentId);
        if(shipment.isPresent())
            return ResponseEntity.ok().body(shipment.get());
        else
            throw new ShipmentNotFound("Shipment not found: " + shipmentId);
    }

    @PostMapping("/shipments")
    public ResponseEntity postShipment(@RequestParam("customer") Long customerId, @RequestParam("book") Integer bookId) throws JsonProcessingException {
        Customer customer = feignClientApiGateway.getCustomerById(customerId);
        Book book = feignClientApiGateway.getBookById(bookId);

        if(book.getStock().getCurrentQuantity() > 0){
            Shipment newShipment = new Shipment(
                    customer.getId(),
                    book.getId(),
                    customer.getFirstName()+" "+customer.getLastName(),
                    customer.getAddress(),
                    customer.getEmail(),
                    LocalDate.now(),
                    null
            );
            Shipment savedShipment = shippingRepository.save(newShipment);
            History newOrderHistory = new History(
                    savedShipment.getId(),
                    savedShipment.getOrderReceivedDate(),
                    book.getStock().getCurrentPrice()
            );
            List<History> orderHistory = customer.getHistory();
            orderHistory.add(newOrderHistory);
            customer.setHistory(orderHistory);

            feignClientApiGateway.updateCustomer(customer);

            book.getStock().setCurrentQuantity(book.getStock().getCurrentQuantity()-1);

            feignClientApiGateway.updateBook(book);

            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequestUri().path("/{id}")
                    .buildAndExpand(savedShipment.getId())
                    .toUri();

            Map<String, Shipment> map = new HashMap<>();
            map.put("shipment", newShipment);
            //  + "\r\n"

            return ResponseEntity.created(location).header("New Shipment", "Created").body(map);

//            HttpHeaders headers = new HttpHeaders();
//            headers.set(HttpHeaders.CONTENT_LENGTH, String.valueOf(new ObjectMapper().writeValueAsString(map).length()));
//            return new ResponseEntity(map, headers, HttpStatus.CREATED);

        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }
}
