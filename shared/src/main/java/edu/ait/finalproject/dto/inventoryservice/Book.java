package edu.ait.finalproject.dto.inventoryservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Entity
@Table(name = "Books")
@ApiModel(description = "Bookshop Book Details - A book entity to add to the bookshop inventory storing book title, author, publisher, publish data, edition and ISBN with current stock details")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Book must have a Title")
    @ApiModelProperty(name = "title", dataType = "String", example = "Microservices in Action", notes="Must have a valid book title")
    private String title;

    @NotBlank(message = "Book must have Author")
    @ApiModelProperty(name = "author", dataType = "String", example = "Morgan Bruce", notes="Must have a valid book Author")
    private String author;

    @NotBlank(message = "Book must have Publisher")
    @ApiModelProperty(name = "publisher", dataType = "String", example = "Manning", notes="Must have a valid book Publisher")
    private String publisher;

    @Column(name = "publish_date", columnDefinition = "DATE")
    @PastOrPresent
    @ApiModelProperty(name = "publishDate", dataType = "Java LocalDate", example = "10/12/2020", notes="Must be past or present date")
    private java.time.LocalDate publishDate;

    @Positive
    @ApiModelProperty(name = "edition", dataType = "Int", example = "1", notes="Must be a positive integer")
    private int edition;

    @NotEmpty
    @ApiModelProperty(name = "isbn", dataType = "String", example = "123456789", notes="Must exist")
    private String isbn;

    @Lob
    @ApiModelProperty(name = "description", dataType = "String", example = "This book is great")
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stock_id", referencedColumnName = "id")
    private Stock stock;

    public Book() {
    }

    public Book(String title, String author, String publisher, LocalDate publishDate, int edition, String isbn, String description, Stock stock) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.publishDate = publishDate;
        this.edition = edition;
        this.isbn = isbn;
        this.description = description;
        this.stock = stock;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

