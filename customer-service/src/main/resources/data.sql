--insert into History (ID,ORDER_COST,ORDER_DATE,ORDER_ID) values
--	(1,120.45, DATE '2020-02-12',1151),
--	(2,150.65, DATE '2021-10-02',1231),
--	(3,10.45, DATE '2019-12-12',1634),
--	(4,12.50, DATE '2018-06-25',2345);
insert into Customers (id,first_name,last_name,email,address) values
	(1,'Mary','Murphy','m-murph@gmail.com','12 Seaview, Clontarf, Dublin 3'),
	(2, 'John', 'O Sullivan', 'jsully@hotmail.com', 'Baile Bhuirne, Macroom, Co. Cork'),
	(3, 'Rita', 'Barrett', 'rita.barrett@gmail.com', 'Newport Road, Castlebar, Co. Mayo');
--insert into customer_history (customer_id, history_id) values
--	(1,1),
--	(1,2),
--	(1,4),
--	(2,3);