package edu.ait.shippingservice.controllers;

import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.shippingservice.Shipment;
import edu.ait.shippingservice.FeignClient.FeignClientApiGateway;
import edu.ait.shippingservice.repository.ShippingRepository;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(ShippingServiceController.class)
class ShippingServiceControllerTest {

    @MockBean
    private ShippingRepository shippingRepository;

    @MockBean
    private FeignClientApiGateway feignClientApiGateway;

    @Autowired
    private MockMvc mockMvc;

    private Shipment shipment;

    private List<Shipment> shipmentList = new ArrayList<>();

    LocalDate testDate1 = LocalDate.of(2000,01,01);
    LocalDate testDate2 = LocalDate.of(2030,01,01);

    @BeforeEach
    void setUp() {
        RestAssuredMockMvc.mockMvc(mockMvc);
        shipment = new Shipment(1L,1,"John Doe", "The Test Address", "john.doe@gamil.com", testDate1, testDate2);
        shipment.setId(1L);
        shipmentList.add(shipment);
    }

    @Test
    void getAllShipments() {

        Mockito.when(shippingRepository.findAll()).thenReturn(shipmentList);

        RestAssuredMockMvc
                .given()
                .when()
                .get("/shipments")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(1))
                .body("[0].bookId", Matchers.equalTo(1))
                .body("[0].customerName", Matchers.equalTo("John Doe"))
                .body("[0].customerAddress", Matchers.equalTo("The Test Address"))
                .body("[0].customerEmail", Matchers.equalTo("john.doe@gamil.com"))
                .body("[0].orderReceivedDate", Matchers.equalTo("2000-01-01"))
                .body("[0].orderCompletionDate", Matchers.equalTo("2030-01-01"));
    }

    @Test
    void getShipmentById() {
        Mockito.when(shippingRepository.findById(1L)).thenReturn(Optional.ofNullable(shipment));

        RestAssuredMockMvc
                .given()
                .when()
                .get("/shipments/1")
                .then()
                .statusCode(200)
                .body("id",Matchers.equalTo(1))
                .body("bookId", Matchers.equalTo(1))
                .body("customerName", Matchers.equalTo("John Doe"))
                .body("customerAddress", Matchers.equalTo("The Test Address"))
                .body("customerEmail", Matchers.equalTo("john.doe@gamil.com"))
                .body("orderReceivedDate", Matchers.equalTo("2000-01-01"))
                .body("orderCompletionDate", Matchers.equalTo("2030-01-01"));
    }

    @Test
    void postShipment() {
        Mockito.when(shippingRepository.save(any(Shipment.class))).thenReturn(shipment);
        RestAssuredMockMvc
                .given()
                .body("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"customerId\": 1,\n" +
                        "    \"bookId\": 1,\n" +
                        "    \"customerName\": \"John Doe\",\n" +
                        "    \"customerAddress\": \"The Test Address\",\n" +
                        "    \"customerEmail\": \"john.doe@gamil.com\",\n" +
                        "    \"orderReceivedDate\": \"2000-01-01\",\n" +
                        "    \"orderCompletionDate\": \"2030-01-01\",\n" +
                        "}")
                .when()
                .post("/shipments")
                .then()
                .statusCode(400);
    }
}