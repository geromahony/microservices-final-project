package edu.ait.customerservice.entities;

import edu.ait.customerservice.repository.CustomerRepository;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.customerservice.History;

import edu.ait.finalproject.dto.inventoryservice.Book;
import edu.ait.finalproject.dto.inventoryservice.Stock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CustomerRepository customerRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Customer customer;
    private History history;
    LocalDate testDate = LocalDate.of(2000,01,01);


    @Before
    public void setUp(){
        history = new History(1L, testDate, 12.34F);
//        history.setId(1L);
//        history.setOrderId(1L);
//        history.setOrderDate(testDate);
//        history.setOrderCost(12.34F);
        //customer = new Customer( 1L,"Jane","Doe","jane.doe@gmail.com","My Test Address, Test Road, Test 1", history);
    }

    @Test
    public void injectedComponentsAreNotNull(){
        assertThat(entityManager).isNotNull();
        assertThat(customerRepository).isNotNull();
    }

    @Test
    public void saveHistoryEntity(){
        History savedHistory = this.entityManager.persistAndFlush(history);
        assertThat(savedHistory.getId()).isNotNull();
        assertThat(savedHistory.getId()).isEqualTo(1L);
        assertThat(savedHistory.getOrderDate().isEqual(testDate));
        assertThat(savedHistory.getOrderCost()).isEqualTo(12.34F);
    }

//    @Test
//    public void saveCustomerEntity(){
//        Customer savedCustomer = this.entityManager.merge(customer);
//        assertThat(savedCustomer.getId()).isNotNull();
//        assertThat(savedCustomer.getId()).isEqualTo(1L);
//        assertThat(savedCustomer.getFirstName()).isEqualTo("Jane");
//        assertThat(savedCustomer.getLastName()).isEqualTo("Doe");
//        assertThat(savedCustomer.getEmail()).isEqualTo("jane.doe@gmail.com");
//        assertThat(savedCustomer.getAddress()).isEqualTo("My Test Address, Test Road, Test 1");
//        assertThat(savedCustomer.getHistory()).isEqualTo(history);
//    }
}
