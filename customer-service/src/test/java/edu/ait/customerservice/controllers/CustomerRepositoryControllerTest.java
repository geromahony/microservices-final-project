package edu.ait.customerservice.controllers;

import edu.ait.customerservice.repository.CustomerRepository;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.finalproject.dto.customerservice.History;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(CustomerRepositoryController.class)
class CustomerRepositoryControllerTest {

    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private MockMvc mockMvc;

    private Customer customer;
    private List<Customer> customerList = new ArrayList<>();

    private History history;
    private List<History> historyList = new ArrayList<>();

    LocalDate testDate = LocalDate.of(2000,01,01);

    @BeforeEach
    void setUp() {
        RestAssuredMockMvc.mockMvc(mockMvc);
        history = new History(1L, testDate, 12.34F);
        historyList.add(history);
        customer = new Customer( 1L,"Jane","Doe","jane.doe@gmail.com","My Test Address, Test Road, Test 1", historyList);
        customerList.add(customer);
    }

    @Test
    void getAllCustomers() {
        Mockito.when(customerRepository.findAll()).thenReturn(customerList);

        RestAssuredMockMvc
                .given()
                .when()
                .get("/customers")
                .then()
                .statusCode(200)
                .body("[0].id",Matchers.equalTo(1))
                .body("[0].firstName", Matchers.equalTo("Jane"))
                .body("[0].lastName", Matchers.equalTo("Doe"))
                .body("[0].email", Matchers.equalTo("jane.doe@gmail.com"))
                .body("[0].address", Matchers.equalTo("My Test Address, Test Road, Test 1"))
                .body("[0].history[0].id", Matchers.equalTo(null))
                .body("[0].history[0].orderId", Matchers.equalTo(1))
                .body("[0].history[0].orderDate", Matchers.equalTo("2000-01-01"))
                .body("[0].history[0].orderCost", Matchers.equalTo(12.34F));
    }

    @Test
    void getCustomerById(){
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.ofNullable(customer));

        RestAssuredMockMvc
                .given()
                .when()
                .get("/customers/1")
                .then()
                .statusCode(200)
                .body("id",Matchers.equalTo(1))
                .body("firstName", Matchers.equalTo("Jane"))
                .body("lastName", Matchers.equalTo("Doe"))
                .body("email", Matchers.equalTo("jane.doe@gmail.com"))
                .body("address", Matchers.equalTo("My Test Address, Test Road, Test 1"))
                .body("history[0].id", Matchers.equalTo(null))
                .body("history[0].orderId", Matchers.equalTo(1))
                .body("history[0].orderDate", Matchers.equalTo("2000-01-01"))
                .body("history[0].orderCost", Matchers.equalTo(12.34F));
    }

    @Test
    void newCustomer() {

        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);
        RestAssuredMockMvc
                .given()
                .contentType("application/json")
                .body("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"firstName\": \"Jane\",\n" +
                        "    \"lastName\": \"Doe\",\n" +
                        "    \"email\": \"jane.doe@gmail.com\",\n" +
                        "    \"address\": \"My Test Address, Test Road, Test 1\",\n" +
                        "    \"history\": [\n" +
                        "        {\n" +
                        "            \"id\": 1,\n" +
                        "            \"orderId\": 1,\n" +
                        "            \"orderDate\": \"2000-01-01\",\n" +
                        "            \"orderCost\": 12.34\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}")
                .when()
                .post("/customers")
                .then()
                .statusCode(201)
                .header("Location", Matchers.containsString("http://localhost/customers/1"));
    }

    @Test
    void updateCustomer() {

        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);
        RestAssuredMockMvc
                .given()
                .contentType("application/json")
                .body("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"firstName\": \"Jane\",\n" +
                        "    \"lastName\": \"Doe\",\n" +
                        "    \"email\": \"jane.doe@gmail.com\",\n" +
                        "    \"address\": \"My Test Address, Test Road, Test 1\",\n" +
                        "    \"history\": [\n" +
                        "        {\n" +
                        "            \"id\": 1,\n" +
                        "            \"orderId\": 1,\n" +
                        "            \"orderDate\": \"2000-01-01\",\n" +
                        "            \"orderCost\": 12.34\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}")
                .when()
                .put("/customers")
                .then()
                .statusCode(200);
    }

    @Test
    void deleteCustomer() {
        Mockito.doNothing().when(customerRepository).deleteById(1L);
        RestAssuredMockMvc
                .given()
                .when()
                .delete("/customers/1")
                .then()
                .statusCode(200);
    }
}