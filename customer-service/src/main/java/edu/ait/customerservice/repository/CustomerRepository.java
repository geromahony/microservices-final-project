package edu.ait.customerservice.repository;

import edu.ait.finalproject.dto.customerservice.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
