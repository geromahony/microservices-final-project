package edu.ait.finalproject.dto.shippingservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Shipments")
@ApiModel(description = "Bookshop Shipment Details - A shipment entity to store customer name, address email with book order and shipment details")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long customerId;

    @NotNull
    private Integer bookId;

    @NotBlank
    @ApiModelProperty(name = "customerName", dataType = "String", example = "John Smith", notes="Must be a valid customer Name")
    private String customerName;

    @NotBlank
    @ApiModelProperty(name = "customerAddress", dataType = "String", example = "John Smith", notes="Must be a valid customer Address")
    private String customerAddress;

    @NotBlank
    @ApiModelProperty(name = "customerEmail", dataType = "String", example = "john-smith@hotmail.com", notes="Must be a valid customer Email")
    private String customerEmail;

    @PastOrPresent
    @ApiModelProperty(name = "orderReceivedDate", dataType = "Java8 LocalDate", example = "10/10/2020", notes="Must be a valid date")
    private java.time.LocalDate orderReceivedDate;

    @FutureOrPresent
    @ApiModelProperty(name = "orderCompletionDate", dataType = "Java8 LocalDate", example = "10/10/2020", notes="Must be a valid date")
    private java.time.LocalDate orderCompletionDate;

    public Shipment() {
    }

    public Shipment(Long customerId, Integer bookId, String customerName, String customerAddress, String customerEmail, LocalDate orderReceivedDate, LocalDate orderCompletionDate) {
        this.customerId = customerId;
        this.bookId = bookId;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerEmail = customerEmail;
        this.orderReceivedDate = orderReceivedDate;
        this.orderCompletionDate = orderCompletionDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public LocalDate getOrderReceivedDate() {
        return orderReceivedDate;
    }

    public void setOrderReceivedDate(LocalDate orderReceivedDate) {
        this.orderReceivedDate = orderReceivedDate;
    }

    public LocalDate getOrderCompletionDate() {
        return orderCompletionDate;
    }

    public void setOrderCompletionDate(LocalDate orderCompletionDate) {
        this.orderCompletionDate = orderCompletionDate;
    }
}