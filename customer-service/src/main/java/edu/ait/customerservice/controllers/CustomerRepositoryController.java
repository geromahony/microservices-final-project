package edu.ait.customerservice.controllers;

import edu.ait.customerservice.exceptions.CustomerNotFoundException;
import edu.ait.finalproject.dto.customerservice.Customer;
import edu.ait.customerservice.repository.CustomerRepository;
import edu.ait.finalproject.dto.inventoryservice.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class CustomerRepositoryController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/customers")
    public List<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@Valid @PathVariable Long id){
        Optional <Customer> theCustomer = customerRepository.findById(id);
        if( theCustomer.isPresent())
            return theCustomer.get();
        else
            throw new CustomerNotFoundException("Unable to find customer with id: " + id);
    }

    @PostMapping("/customers")
    public ResponseEntity newCustomer(@Valid @RequestBody Customer customer) {
        Customer newCustomer = customerRepository.save(customer);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(newCustomer.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/customers")
    public ResponseEntity updateCustomer(@Valid @RequestBody Customer customer) {

        if (customer.getId() != null) {
            customerRepository.save(customer);

            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            Customer savedCustomer = customerRepository.save(customer);

            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest().path("/{id}")
                    .buildAndExpand(savedCustomer.getId())
                    .toUri();
            return ResponseEntity.created(location).build();
        }
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomer(@Valid @PathVariable(value = "id") Long customerId) throws CustomerNotFoundException {

        try {
            customerRepository.deleteById(customerId);
        } catch (EmptyResultDataAccessException e) {
            throw new CustomerNotFoundException("Unable to delete customer with id: " + customerId);
        }
    }

}
